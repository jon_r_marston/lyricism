import unittest

from lyrics.exceptions import LyricsNotAvailable
from lyrics.webservice import get_song_lyrics, word_count


class LyricsTest(unittest.TestCase):

    def setUp(self) -> None:
        self.lyric_songs = [
            ('Metallica', 'Enter Sandman'),
            ('Fear Factory', 'Demanufacture'),
            ('Ed Sheeran', 'Bloodstream'),
            ('Oasis', 'Acquiesce')
        ]

        self.instrumentals = [
            ('Fleetwood Mac', 'Albatross'),
            ('Metallica', 'Orion'),
            ('Van Halen', 'Eruption')
        ]

    def test_http_sanity(self):
        """
        Invalid song title (I hope!)
        :return:
        """
        with self.assertRaises(LyricsNotAvailable):
            get_song_lyrics("ShostakovichHootenannyThe3rd", "Jailhouse Rock ver. 2")

    def test_lyric_len(self):
        """
            Lyric length
        :return:
        """
        for song in self.lyric_songs:
            lyr = get_song_lyrics(song[0], song[1])
            self.assertGreater(len(lyr), 0, "%s - %s" % (song[0], song[1]))
            self.assertNotEqual(lyr, "(Instrumental)", "%s - %s" % (song[0], song[1]))

    def test_instrumentals(self):
        """
            Instrumentals - should allow for variations of "Instrumental".
        :return:
        """
        for song in self.instrumentals:
            self.assertIsNone(word_count(song[0], song[1]), "%s - %s" % (song[0], song[1]))
