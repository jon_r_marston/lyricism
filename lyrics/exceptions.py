class WebServiceError(Exception):

    def __init__(self, message: str):
        super().__init__("WS request failed with: %s" % (message,))


class LyricsNotAvailable(Exception):

    def __init__(self):
        super()


class InvalidLyricsResponse(Exception):

    def __init__(self, message: str):
        super().__init__("WS request failed with: %s" % (message,))
