import json
import re
from json import JSONDecodeError

import requests

from lyrics.exceptions import LyricsNotAvailable, InvalidLyricsResponse

BASE_URL = "https://api.lyrics.ovh/v1/"


def get_song_lyrics(artist: str, title: str) -> str:

    """
    Return the lyrics for the provided song as a single string.
    :param artist:
    :param title:
    :return:
    """

    try:
        r = requests.get("%s%s/%s/" % (BASE_URL, artist, title))
        r.raise_for_status()

        return json.loads(r.text)['lyrics']

    except requests.HTTPError as hte:
        raise LyricsNotAvailable()
    except JSONDecodeError as jde:
        raise InvalidLyricsResponse(str(jde))
    except KeyError as ke:
        raise InvalidLyricsResponse("Lyrics missing from response: %s" % (ke,))


def word_count(artist: str, title: str):

    """
        Return the nunmber of words in the provided song as an integer or None in the following edge-cases:
        * the recording is an instrumental
        * the lyrics is an empty string

    :param artist:
    :param title:
    :return:
    """

    try:
        lyr = get_song_lyrics(artist, title)
        # Ignore variations on the string "Instrumental".
        if re.match('^[\(\[]*[Ii]nstrumental[\)\]]*$', lyr) is not None:
            # An instrumental isn't a song anyway.
            raise LyricsNotAvailable()
        cnt = len(lyr.split())
        if cnt == 0:
            raise LyricsNotAvailable()
        return cnt
    except LyricsNotAvailable:
        return None
