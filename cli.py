#!/usr/bin/env python
import sys

from mb.exceptions import BadTasteException
from mb.webservice import fetch_artist

if len(sys.argv) > 1:

    artists = []

    for artist_param in sys.argv[1:]:

        try:

            artist = fetch_artist(artist_param)

            if artist not in artists:
                artists.append(artist)
                artist.build_stats()

        except BadTasteException:
            print("You may not search for %s" % (artist_param,))
            continue

    artists.sort(reverse=True)  # overloaded comparison operators require that stats have been loaded

    for sorted_artist in artists:
        print("Artist: %s =======" % (sorted_artist.name,))

        print("Verbosity index (mean): %s" % (sorted_artist.get_stat("verbosity_index"),))
        print("Median number of words: %s" % (sorted_artist.get_stat("median"),))
        print("Standard deviation: %s" % (sorted_artist.get_stat("stdev"),))
        print("Min words: %s" % (sorted_artist.get_stat("min"),))
        print("Max words: %s" % (sorted_artist.get_stat("max"),))

        print("=======================")

else:

    print("Missing artist name")
