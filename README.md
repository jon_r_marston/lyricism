# Lyricism

Statistics related to song lyrics.

## Installation

### Dependencies

This package requires **Python 3.8** or above and uses the requests library.

### Install requirements
`pip install -r requirements.txt`

## Usage

The interface for this code is simple the cli.py script. You can pass in one (or more) artist names as separate 
command line arguments. Make sure you quote names with multiple words.

E.g.:

`python cli.py metallica "system of a down" "ed sheeran"`

The script will output a section for each artist, which details the collected stats. The mean number of 
words in the songs is referred to as "verbosity index". Stats are rounded to the nearest integer.

The artists are sorted by decreasing verbosity index.

Also, don't search for Elbow, The Smiths or The Spice Girls.

## Notes

### Performance

The lyrics API is, indeed, slow. This program uses a few methods to cut down on waiting times:
* stats are generated using a method and cached within the model.
* song lists from MusicBrainz are cached.
* duplicate titles are removed to minimise requests to teh lyrics API.

I would really have liked to put some async request handling into this application but unfortunately time is an issue 
for me and the relative increase in complexity would take considerably longer to implement. Also, there doesn't seem to be any 
documentation regarding fair use and request limits on the lyrics API; running multiple 
parallel requests might introduce API instability.