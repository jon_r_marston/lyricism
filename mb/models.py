from functools import cached_property
from statistics import mean, median, stdev
from typing import List

import mb.webservice
from lyrics.webservice import word_count
from mb.exceptions import BadTasteException, StatsNotLoaded, InvalidStatName

BAD_GUYS = {
    'Elbow': True,
    'The Smiths': True,
    'Spice Girls': True
}


class Artist(object):

    """
        Represents an artist as identified by a MusicBrainz ID and name.
    """

    def __eq__(self, other):
        return self.get_arid() == other.get_arid()

    def __ge__(self, other):
        return self.get_stat("verbosity_index") >= other.get_stat("verbosity_index")

    def __gt__(self, other):
        return self.get_stat("verbosity_index") > other.get_stat("verbosity_index")

    def __le__(self, other):
        return self.get_stat("verbosity_index") <= other.get_stat("verbosity_index")

    def __lt__(self, other):
        return self.get_stat("verbosity_index") < other.get_stat("verbosity_index")

    def __init__(self, mb_id, name):
        self._mb_id = mb_id
        self.name = name

        self._my_song_cache = None

        # We do the search first so the service will normalise the name into something we can predict.
        # We have to get this right, catching bad taste is *important*.
        taste_alert = BAD_GUYS.get(self.name, None)
        if taste_alert is not None:
            raise BadTasteException()

        self._verbosity_index = None
        self._median = None
        self._stdev = None
        self._min = None
        self._max = None

    def get_stat(self, stat_name):
        privatised = "_%s" % (stat_name,)
        if hasattr(self, privatised):
            stat = getattr(self, privatised)
            if stat is not None:
                return stat
            else:
                raise StatsNotLoaded(self, stat_name)
        else:
            raise InvalidStatName(stat_name)

    def __str__(self):
        return self.name

    def get_arid(self) -> str:
        """
            Returns the MusicBrainz artist id.
        :return:
        """
        return self._mb_id

    def build_stats(self):
        """
            This method is expensive and therefore isn't called from __init__, but the stats are required
            for an Artist instance to function correctly. Many operations on this object will raise a StatsNotLoaded
            exception unless this method has been called.
        :return:
        """
        counts = []
        for song in self.my_songs:
            cnt = word_count(self.name, song)
            if cnt is not None:
                counts.append(cnt)
        self._verbosity_index = int(mean(counts))
        self._median = int(median(counts))
        self._stdev = int(stdev(counts))
        self._min = int(min(counts))
        self._max = int(max(counts))

    @cached_property
    def my_songs(self) -> List[str]:
        """
        Returns a list of song title strings.
        This is 'expensive' to fetch and is therefore cached; we're assuming that either:
        1) the band hasn't released a new song during the lifetime of this instance
        2) if new songs are released, the number of words in them exactly matches the mean and we'll be ok :-/
        :return:
        """
        return mb.webservice.list_songs(self._mb_id)
