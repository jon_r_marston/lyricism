import unittest

import mb.models
from mb.exceptions import StatsNotLoaded, InvalidStatName


class MusicBrainzModelTest(unittest.TestCase):

    def setUp(self) -> None:
        self.artists = [
            ('System of a down', 'cc0b7089-c08d-4c10-b6b0-873582c17fd6'),
            ('London Grammar', '4dbe08c1-b40a-43b3-ab89-210000bd8ab2'),
            ('Hayseed Dixie', 'fea8db99-8c38-4ee3-bd20-36fb5e3a0839')
        ]
        # Ed says way more than Metallica. We know this.
        self.verbose = ("Ed Sheeran", "b8a7c51f-362c-4dcb-a259-bc6e0095f0a6")
        self.terse = ("Metallica", "65f4f0c5-ef9e-490c-aee3-909e7ae6b2ab")

    def test_stats(self):
        for artist_test in self.artists:
            a = mb.models.Artist(artist_test[1], artist_test[0])
            a.build_stats()

    def test_no_stats(self):
        a = mb.models.Artist(self.artists[0][1], self.artists[0][0])
        with self.assertRaises(StatsNotLoaded):
            a.get_stat("verbosity_index")

    def test_invalid_stat_name(self):
        a = mb.models.Artist(self.artists[0][1], self.artists[0][0])
        a.build_stats()
        with self.assertRaises(InvalidStatName):
            a.get_stat("expletives_per_minute")

    def test_comparisons(self):
        v = mb.models.Artist(self.verbose[1], self.verbose[0])
        v.build_stats()
        t = mb.models.Artist(self.terse[1], self.terse[0])
        t.build_stats()

        self.assertGreater(v, t)
        self.assertEqual(v, v)
        self.assertLess(t, v)
