class MBBaseException(Exception):

    def __init__(self, msg: str):
        self.msg = msg

    def __str__(self):
        return self.msg


class UnknownArtist(MBBaseException):

    def __init__(self, artist_name: str):
        super().__init__(artist_name)


class WebServiceError(MBBaseException):

    def __init__(self, message: str):
        super().__init__("WS request failed with: %s" % (message,))


class BadTasteException(MBBaseException):

    def __init__(self):
        super().__init__("I am not polluting that poor, innocent webservice with any more of your filth")


class StatsNotLoaded(MBBaseException):

    """
        This is raised by accesses to stats either directly using the 'get' method or indirectly by
        the overloaded comparison operators.
    """

    def __init__(self, artist, stat):
        super().__init__("No stats have been loaded for artist %s when accessing %s" % (artist, stat))


class InvalidStatName(MBBaseException):

    def __init__(self, stat):
        super().__init__("Invalid stat name %s" % (stat,))
