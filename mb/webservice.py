import itertools

import requests
import xml.etree.ElementTree as ET
from typing import List

from mb.exceptions import WebServiceError, UnknownArtist
import mb.models

WS_BASE_URL = "https://musicbrainz.org/ws/2/"
WS_XMLNS = "http://musicbrainz.org/ns/mmd-2.0#"

NS = {
    'mmd': "http://musicbrainz.org/ns/mmd-2.0#"
}


def fetch_artist(artist_name: str):

    try:
        r = requests.get("%sartist/?query=artist:%s" % (WS_BASE_URL, artist_name,))
        r.raise_for_status()

        root_doc = ET.fromstring(r.text)

        artist_list = root_doc.find("mmd:artist-list", NS)
        if artist_list is None:
            raise WebServiceError("Missing artist list")

        artist_element = None

        try:
            if int(artist_list.get("count")) > 0:
                artist_element = artist_list[0]
            else:
                raise UnknownArtist(artist_name)
        except (ValueError, TypeError):  # non-integer artist count
            raise WebServiceError("Invalid artist count")

        try:
            return mb.models.Artist(artist_element.get("id"), artist_element.find("mmd:name", NS).text)
        except AttributeError:  # missing attributes come back as None and trigger this
            raise WebServiceError("Invalid artist syntax")

    except requests.HTTPError as hte:
        raise WebServiceError("Musicbrainz service error: %s" % (hte,))
    except requests.ConnectionError as ce:
        raise WebServiceError("Connect fail: %s" % (ce,))
    except ET.ParseError as pe:
        raise WebServiceError("Invalid XML: %s" % (pe,))


def _do_song_query(arid: str, offset: int = 0) -> tuple:
    """
    Perform the actual web service search using the provided ARIN and offset.

    Returns a three-tuple of (track names, total number of tracks by artist, number fetched)
    :param arid:
    :param offset:
    :return:
    """

    base_url = "%srecording/?query=arid:%s AND video:false AND country:GB and status:official&limit=100&offset=%s" \
               % (WS_BASE_URL, arid, offset)

    ret = []

    try:
        r = requests.get(base_url)
        r.raise_for_status()

        root_doc = ET.fromstring(r.text)

        seen_songs = 0

        try:
            song_count = int(root_doc.find("./mmd:recording-list", NS).get('count', None))
        except (TypeError, ValueError, AttributeError):
            raise WebServiceError("Song count invalid")

        for recording in root_doc.findall("./mmd:recording-list/mmd:recording", NS):

            seen_songs += 1

            if recording.find("./mmd:disambiguation", NS) is not None:
                continue

            title_ele = recording.find("./mmd:title", NS)
            if title_ele is not None:
                ret.append(title_ele.text)

        return ret, song_count, seen_songs

    except requests.HTTPError as hte:
        raise WebServiceError("Musicbrainz service error: %s" % (hte,))


def list_songs(arid) -> List[str]:

    """
    A full list of official, uk, non-video recordings for this artist. From my tests, this seems to include album tracks
    and singles. Dodgy track names, will simply get a 404 from the lyrics service and we fettle out duplicates.
    :param arid:
    :return:
    """

    ret, total_songs, seen_songs = _do_song_query(arid, 0)

    while seen_songs < total_songs:
        more_songs, total_songs, add_count = _do_song_query(arid, seen_songs)
        seen_songs += add_count
        ret = itertools.chain(ret, more_songs)

    # fettle out duplicates
    return list(dict.fromkeys(ret))

