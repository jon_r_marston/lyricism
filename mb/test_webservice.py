import unittest

import mb.webservice
from mb.exceptions import BadTasteException, UnknownArtist


class MusicBrainzTest(unittest.TestCase):

    def setUp(self) -> None:
        self.artists = [
            ('System of a down', 'cc0b7089-c08d-4c10-b6b0-873582c17fd6'),
            ('London Grammar', '4dbe08c1-b40a-43b3-ab89-210000bd8ab2'),
            ('Hayseed Dixie', 'fea8db99-8c38-4ee3-bd20-36fb5e3a0839')
        ]
        self.musical_atrocities = ["Elbow", "The Smiths"]

    def test_artist_lookup_positive(self):
        for artist_test in self.artists:
            artist = mb.webservice.fetch_artist(artist_test[0])
            self.assertEqual(artist.get_arid(), artist_test[1], artist_test[0])

    def test_artist_lookup_negative(self):
        with self.assertRaises(UnknownArtist):
            mb.webservice.fetch_artist("DodgyDaveAndTheHolmfirthBlacksmiths")

    def test_fetches_songs(self):
        """
            A simple test that number of songs > 10 (they're all groups with lots of songs),
            to test http/parsing etc.
        :return:
        """
        for artist_test in self.artists:
            self.assertGreater(len(mb.webservice.list_songs(artist_test[1])), 10, "artist_test[0] has too few songs")

    def test_daily_mail_readers(self):
        """
            Make sure the bad taste filter is functioning as expected.
        :return:
        """
        with self.assertRaises(BadTasteException):
            for bad in self.musical_atrocities:
                mb.webservice.fetch_artist(bad)

